import tensorflow as tf
from tqdm import tqdm

from augumentation import augment
from ciphar_dataset import CifarDataset

LEARNING_RATE = 3e-4


def get_logits(features, model_params):
    filters = model_params.get('filters', 64)
    kernel = model_params.get('kernel', [5, 5])

    c1_logits = tf.layers.conv2d(features, filters=filters,
                                 kernel_size=kernel, strides=[1, 1],
                                 use_bias=False)
    c1_norm = tf.nn.relu(tf.layers.batch_normalization(c1_logits))
    m1 = tf.layers.max_pooling2d(c1_norm, pool_size=[3, 3], strides=[2, 2])

    c2_logits = tf.layers.conv2d(m1, filters=filters, kernel_size=kernel,
                                 strides=[1, 1], use_bias=False)
    c2_norm = tf.nn.relu(tf.layers.batch_normalization(c2_logits))
    m2 = tf.layers.max_pooling2d(c2_norm, pool_size=[3, 3], strides=[2, 2])

    y3 = tf.layers.flatten(m2)

    d1_logits = tf.layers.dense(y3, units=384, use_bias=False)
    d1_norm = tf.nn.relu(tf.layers.batch_normalization(d1_logits))
    d2_logits = tf.layers.dense(d1_norm, units=192, use_bias=False)
    d2_norm = tf.nn.relu(tf.layers.batch_normalization(d2_logits))

    out_logits = tf.layers.dense(d2_norm, units=10)
    out_norm = tf.layers.batch_normalization(out_logits)
    return out_norm


def get_model(labels, logits):
    classes = tf.argmax(logits, 1)
    accuracy = tf.reduce_mean(tf.cast(tf.equal(classes, labels), 'float'))
    loss = tf.losses.sparse_softmax_cross_entropy(logits=logits, labels=labels)
    train = tf.train.AdamOptimizer(LEARNING_RATE).minimize(loss)
    return accuracy, loss, train


def init_model_and_train(data,
                         epochs,
                         rt_augmentation=False,
                         log_dir_path='/tmp/cifar/',
                         model_dump='/tmp/model.ckpt',
                         log_interval=None,
                         model_params=None):
    tf.reset_default_graph()

    X = tf.placeholder(tf.float32, [None, *data.img_shape], name='x')
    Y = tf.placeholder(tf.int64, [None], name='labels')

    logits = get_logits(X, model_params)
    accuracy, loss, train = get_model(Y, logits)

    saver = tf.train.Saver()
    test_feed = {X: data.x_test, Y: data.y_test}

    tf.summary.scalar('Loss', loss)
    tf.summary.scalar('Accuracy', accuracy)
    write_op = tf.summary.merge_all()

    writer_train = tf.summary.FileWriter(log_dir_path + 'train')
    writer_val = tf.summary.FileWriter(log_dir_path + 'val')

    if rt_augmentation:
        images = tf.placeholder(tf.float32,
                                shape=(None, None, None, 3))
        labels = tf.placeholder(tf.int64, shape=None)

        aug_images, aug_labels = augment(images,
                                         labels,
                                         horizontal_flip=True,
                                         rotate=15,
                                         crop_probability=0.8)

    init = tf.global_variables_initializer()

    with tf.Session() as sess:
        writer_train.add_graph(sess.graph)

        sess.run(init)

        for e in range(epochs):
            with tqdm(total=data.num_batches) as tqdm_bar:
                tqdm_bar.set_description(f'Epoch: {e}')

                for i in range(data.num_batches):
                    x_batch, y_batch = data.next_batch()

                    if rt_augmentation:
                        x_batch, y_batch = sess.run(
                            [aug_images, aug_labels],
                            feed_dict={
                                images: x_batch,
                                labels: y_batch
                            })

                    batch_train_feed = {X: x_batch, Y: y_batch}

                    if log_interval and i % log_interval == 0:
                        step = e * data.num_batches + i

                        summary, _ = sess.run([write_op, train],
                                              feed_dict=batch_train_feed)
                        writer_train.add_summary(summary, step)

                        summary = sess.run(write_op, feed_dict=test_feed)
                        writer_val.add_summary(summary, step)
                    else:
                        _ = sess.run(train, feed_dict=batch_train_feed)

                    tqdm_bar.update(1)
        saver.save(sess, model_dump)


if __name__ == '__main__':
    epochs = 10
    batch_size = 32

    m_params = {'filters': 64,
                'kernel': [3, 3]}

    logdir_str = '-'.join([k + '_' + str(v) for k, v in m_params.items()])

    data = CifarDataset(batch_size, reshuffle_per_epoch=True)
    init_model_and_train(data, epochs,
                         model_params=m_params,
                         log_interval=100,
                         log_dir_path=f"../logs/{logdir_str}/")
