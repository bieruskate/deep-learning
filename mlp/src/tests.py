from ciphar_dataset import CifarDataset
from model import MLP

cifar = CifarDataset(batch_size=32, reshuffle_per_epoch=True,
                     greyscale=False)


def run_with_presets(conf_dict):
    epochs = conf_dict['epochs']
    batch_size = conf_dict['batch_size']
    use_dropout = conf_dict['dropout'] > 0
    keep_prob = conf_dict['dropout']
    l2 = conf_dict['l2']
    arch = conf_dict['architecture']
    optimizer = conf_dict['optimizer']

    cifar.set_batch_size(batch_size)

    acts = [*['relu'] * (len(arch) - 1), None]
    drop = [*[use_dropout] * (len(arch) - 1), False]

    arch_str = '-'.join([str(a) for a in arch[:-1]])

    log_run_dir = f'Batch({batch_size})-Epochs({epochs})-Drop({keep_prob})-' \
                  f'arch({arch_str})-O({optimizer})-L2({l2})'
    print(log_run_dir)
    log_dir_path = f'/tmp/research/{log_run_dir}/'

    mlp = MLP(input_shape=cifar.img_shape, architecture=arch, activations=acts,
              dropouts=drop,
              initializer='he')
    mlp.compile(optimizer=optimizer, l2=l2)
    mlp.fit(dataset=cifar, epochs=epochs, log_dir_path=log_dir_path,
            keep_prob=keep_prob, log_every_batches=100)


def run_preset_on_iterable(conf_dict, param_key, iterable):
    preset_copy = dict(conf_dict)
    for p in iterable:
        preset_copy['architecture'] = conf_dict['architecture'].copy()
        preset_copy[param_key] = p
        run_with_presets(preset_copy)


if __name__ == '__main__':
    n_outputs = 10

    c_dict = {
        'epochs': 1,
        'batch_size': 128,
        'dropout': 0,
        'l2': False,
        'architecture': [256, n_outputs],
        'optimizer': 'adam'
    }

    architectures = [
        [64], [128], [256], [512],
        [64, 64], [64, 128], [128, 256],
        [64, 256, 64], [64, 128, 256]
    ]
    batch_sizes = [4, 16, 32, 64, 128]
    epochs_set = [2, 4, 8, 16, 32, 128]
    dropouts = [0.95, 0.9, 0.8, 0.7, 0.6]
    l2s = [True, False]
    optimizers = ['adam', 'sgd']

    run_preset_on_iterable(c_dict, 'architecture',
                           map(lambda a: [*a, n_outputs], architectures))
    run_preset_on_iterable(c_dict, 'batch_size', batch_sizes)
    run_preset_on_iterable(c_dict, 'epochs', epochs_set)
    run_preset_on_iterable(c_dict, 'dropout', dropouts)
    run_preset_on_iterable(c_dict, 'l2', l2s)
    run_preset_on_iterable(c_dict, 'optimizer', optimizers)

