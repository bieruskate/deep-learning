import numpy as np
from tensorflow.python.keras.datasets import cifar10

from utils import rgb2gray


class CifarDataset:
    def __init__(self, batch_size, greyscale=False, normalize=True,
                 reshuffle_per_epoch=False, flatten_x=False):
        self.img_shape = [32, 32, 3]
        self.batch_size = batch_size
        self.reshuffle_per_epoch = reshuffle_per_epoch
        self.batch_gen = None
        self.num_batches = 0

        (self.x_train, self.y_train), (self.x_test, self.y_test) = \
            cifar10.load_data()

        if greyscale:
            self._convert_to_greyscale()
            self.img_shape = self.img_shape[:2]

        if normalize:
            self._normalize()

        if flatten_x:
            self._flatten_x()
        self._flatten_y()

        self.num_train_samples = self.x_train.shape[0]

        self.set_batch_size(batch_size)

    def next_batch(self):
        return next(self.batch_gen)

    def set_batch_size(self, batch_size):
        self.batch_gen = self._get_batch_generator()
        self.num_batches = int(np.ceil(self.num_train_samples / batch_size))

    def _convert_to_greyscale(self):
        self.x_train, self.x_test = rgb2gray(self.x_train) / 255.0, \
                                    rgb2gray(self.x_test) / 255.0

    def _normalize(self):
        self.x_train, self.x_test = self.x_train / 255.0, self.x_test / 255.0

    def _flatten_x(self):
        flat_size = np.prod(self.img_shape[:2])
        self.x_train = self.x_train.reshape([-1, flat_size])
        self.x_test = self.x_test.reshape([-1, flat_size])

    def _flatten_y(self):
        self.y_train = self.y_train.reshape(-1)
        self.y_test = self.y_test.reshape(-1)

    def _get_batch_generator(self):
        splits = self._get_random_splits()

        while True:
            for split in splits:
                yield self.x_train[split], self.y_train[split]

            if self.reshuffle_per_epoch:
                splits = self._get_random_splits()

    def _get_random_splits(self):
        arr = np.arange(self.num_train_samples)
        np.random.shuffle(arr)
        return np.split(arr, np.arange(self.num_train_samples,
                                       step=self.batch_size)[1:])
