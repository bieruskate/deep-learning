from functools import partial

import numpy as np
import tensorflow as tf
from tqdm import tqdm


class MLP:
    def __init__(self, input_shape, architecture, activations, initializer,
                 dropouts, dump_path='/tmp/model.ckpt'):
        tf.reset_default_graph()

        self.input_shape = input_shape
        self.architecture = architecture
        self.activations = activations
        self.weight_initializer = self._get_weights_initializer(initializer)
        self.dropouts = dropouts
        self.dump_path = dump_path

        assert len(architecture) == len(activations) == len(dropouts)

        self.weights, self.biases = self._build_architecture()

        self.X = tf.placeholder(tf.float32, [None, *input_shape], name='x')
        tf.summary.image('images', self.X)

        self.Y = tf.placeholder(tf.int64, [None], name='labels')
        self.keep_prob = tf.placeholder(tf.float32, name='dropout_prob')

        self._out = None
        self._train = None
        self._loss = None
        self._accuracy = None

    def compile(self, optimizer='adam', l_rate=1e-3, l2=True, l2_beta=0.001):
        layer = tf.contrib.layers.flatten(self.X)

        params_iterator = zip(self.activations,
                              self.weights.values(),
                              self.biases.values(),
                              self.dropouts)

        for i, (act, weights, bias, has_dropout) in enumerate(params_iterator):
            layer = self._fully_connected(layer, weights, bias, act,
                                          name=f'fc{i + 1}')

            if has_dropout:
                layer = tf.nn.dropout(layer, keep_prob=self.keep_prob)

        with tf.name_scope('xent'):
            self._out = layer
            self._loss = tf.losses.sparse_softmax_cross_entropy(labels=self.Y,
                                                                logits=self._out)

            if l2:
                l2_term = tf.add_n(
                    [tf.nn.l2_loss(v) for v in self.weights.values()]) * l2_beta
                self._loss += l2_term

        with tf.name_scope('train'):
            optimizer = self._get_optimizer(optimizer, lr=l_rate, wd=l2_beta)
            self._train = optimizer.minimize(self._loss)

        self._accuracy = self._get_accuracy()

    @staticmethod
    def _fully_connected(layer_in, weights, bias, activation, name='fc'):
        with tf.name_scope(name):
            fc = tf.matmul(layer_in, weights) + bias

            if activation:
                fc = getattr(tf.nn, activation)(fc)
                tf.summary.histogram(f'activations_{name}', fc)

            tf.summary.histogram('weights', weights)
            tf.summary.histogram('biases', bias)

        return fc

    def fit(self, dataset, epochs, keep_prob, log_dir_path='/tmp/tsblogs',
            log_validation=True, log_every_batches=None):

        init = tf.global_variables_initializer()

        saver = tf.train.Saver()

        with tf.Session() as sess:
            tf.summary.scalar('loss', self._loss)
            tf.summary.scalar('acc', self._accuracy)

            write_op = tf.summary.merge_all()

            writer_train = tf.summary.FileWriter(log_dir_path + 'train')
            writer_train.add_graph(sess.graph)

            if log_validation:
                writer_val = tf.summary.FileWriter(log_dir_path + 'val')
            else:
                writer_val = None

            sess.run(init)

            train_feed_dict = {
                self.X: dataset.x_train,
                self.Y: dataset.y_train,
                self.keep_prob: 1.
            }

            for e in range(epochs):
                with tqdm(total=dataset.num_batches) as tqdm_bar:
                    tqdm_bar.set_description(f'Epoch: {e}')

                    for i in range(dataset.num_batches):
                        x_batch, y_batch = dataset.next_batch()

                        _ = sess.run(self._train,
                                     feed_dict={self.X: x_batch,
                                                self.Y: y_batch,
                                                self.keep_prob: keep_prob})

                        if log_every_batches and i % log_every_batches == 0:
                            step = e * dataset.num_batches + i

                            summary = sess.run(write_op,
                                               feed_dict=train_feed_dict)

                            writer_train.add_summary(summary, step)

                            if log_validation:
                                summary = sess.run(write_op,
                                                   feed_dict={
                                                       self.X: dataset.x_test,
                                                       self.Y: dataset.y_test,
                                                       self.keep_prob: 1.
                                                   })

                                writer_val.add_summary(summary, step)

                        tqdm_bar.update(1)

                    cur_loss, curr_acc = sess.run([self._loss, self._accuracy],
                                                  feed_dict=train_feed_dict)

                    testing_accuracy = self._get_testing_accuracy(dataset)
                    tqdm_bar.set_postfix(loss=cur_loss,
                                         train=curr_acc,
                                         test=testing_accuracy)

                    saver.save(sess, self.dump_path)

    def predict(self, dataset):
        saver = tf.train.Saver()

        with tf.Session() as sess:
            saver.restore(sess, self.dump_path)

            _y = self._out
            if self.activations[-1] is None:
                _y = tf.nn.softmax(self._out)

            prediction = tf.argmax(_y, 1)
            preds = sess.run(prediction, feed_dict={self.X: dataset.x_test,
                                                    self.keep_prob: 1.})

        return preds

    def _get_testing_accuracy(self, dataset):
        testing_accuracy = self._accuracy.eval(
            feed_dict={self.X: dataset.x_test,
                       self.Y: dataset.y_test,
                       self.keep_prob: 1.}
        )
        return testing_accuracy

    def _build_architecture(self):
        weights = {}
        biases = {}

        zeros_init = tf.initializers.zeros()

        self.architecture.insert(0, np.prod(self.input_shape))

        for i, (s1, s2) in enumerate(
                zip(self.architecture, self.architecture[1:])):
            w_name, b_name = f'W{i + 1}', f'B{i + 1}'

            with tf.name_scope(w_name):
                weights[w_name] = tf.Variable(self.weight_initializer([s1, s2]))

            with tf.name_scope(b_name):
                biases[b_name] = tf.Variable(zeros_init([s2]))

        return weights, biases

    def _get_accuracy(self):
        with tf.name_scope('accuracy'):
            _y = self._out
            if self.activations[-1] is None:
                _y = tf.nn.softmax(self._out)

            correct_prediction = tf.equal(tf.argmax(_y, 1), self.Y)
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, 'float'))
        return accuracy

    def _get_weights_initializer(self, initializer):
        return {
            'xavier': tf.glorot_normal_initializer(),
            'he': tf.contrib.layers.variance_scaling_initializer(factor=2.0,
                                                                 mode='FAN_IN',
                                                                 uniform=False),
            'weird_mnist': self.weird_mnist_initializer
        }[initializer]

    @staticmethod
    def weird_mnist_initializer(shape):
        s1 = shape[0]
        return partial(tf.truncated_normal, shape=shape,
                       stddev=1.0 / np.sqrt(float(s1)))

    @staticmethod
    def _get_optimizer(optimizer, lr=1e-3, wd=1e-3):
        return {
            'adam': tf.train.AdamOptimizer(learning_rate=lr),
            'adamw': tf.contrib.opt.AdamWOptimizer(wd, lr),
            'sgd': tf.train.GradientDescentOptimizer(learning_rate=lr)
        }[optimizer]

    def __str__(self):
        return f'Weights:\n{self.weights}\nBiases:\n{self.biases}'
