from matplotlib import pyplot as plt

from ciphar_dataset import CifarDataset
from model import MLP

from sklearn.metrics import confusion_matrix
import numpy as np

from utils import plot_confusion_matrix

if __name__ == '__main__':
    epochs = 1
    batch_size = 16

    cifar = CifarDataset(batch_size=batch_size, reshuffle_per_epoch=True,
                         greyscale=False)

    input_shape = cifar.img_shape
    output_shape = 10

    use_dropout = False

    arch = [64, output_shape]
    acts = [*['relu'] * (len(arch) - 1), None]
    drop = [*[use_dropout] * (len(arch) - 1), False]

    arch_str = '-'.join([str(a) for a in arch[:-1]])
    log_run_dir = f'Batch({batch_size})-Epochs({epochs})-Drop({drop})' \
                  f'-arch({arch_str})'
    log_dir_path = f'/tmp/research/{log_run_dir}/'

    mlp = MLP(input_shape=input_shape, architecture=arch, activations=acts,
              dropouts=drop, initializer='weird_mnist')
    mlp.compile(optimizer='adam', l2=False)
    # mlp.fit(dataset=cifar, epochs=epochs, log_dir_path=log_dir_path,
    #         keep_prob=1, log_every_batches=None)

    mlp.dump_path = '../model_dump/model.ckpt'

    y_pred = mlp.predict(cifar)
    cm = confusion_matrix(cifar.y_test, y_pred)

    names = np.array(
        ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog', 'frog',
         'horse', 'ship', 'truck'])

    plot_confusion_matrix(cm, classes=names,
                          title='Macierz pomyłek')
    plt.savefig('../images/confusion_matrix.pdf', format='pdf')

    missed_mask = y_pred != cifar.y_test

    accuracy = np.mean(~missed_mask)
    print(accuracy)

    missed_images = cifar.x_test[missed_mask]
    missed_labels = cifar.y_test[missed_mask]
    missed_preds = y_pred[missed_mask]

    rand_img_indices = np.random.choice(len(missed_images), 6, replace=False)
    missed_x = missed_images[rand_img_indices]
    missed_label = names[missed_labels[rand_img_indices]]
    predicted_label = names[missed_preds[rand_img_indices]]

    _, axes = plt.subplots(nrows=2, ncols=3)

    for i, row in enumerate(axes):
        img_index = i * len(row)

        for j, ax in enumerate(row):
            index = img_index + j
            ax.imshow(missed_x[index], interpolation='spline16')
            ax.set_title(
                f'T: {missed_label[index]} P: {predicted_label[index]}')

    plt.savefig('../images/missed_images.pdf', format='pdf')
