from pathlib import Path
import time

import matplotlib.pyplot as plt
import tensorflow as tf
from sklearn.metrics import confusion_matrix
from tensorflow.python.ops.rnn import dynamic_rnn
from tqdm import tqdm

from dataset import DataSet
from utils import plot_confusion_matrix


def get_logits(inputs, cell_size, n_layers, n_outputs, cell_class):
    multi_cell = tf.nn.rnn_cell.MultiRNNCell(
        [cell_class(cell_size) for _ in range(n_layers)])

    outputs, _ = dynamic_rnn(multi_cell, inputs, dtype=tf.float32)

    with tf.name_scope('last_output'):
        output = tf.transpose(outputs, [1, 0, 2])
        last = tf.gather(output, int(output.get_shape()[0]) - 1)

    return tf.layers.dense(last, units=n_outputs)


def get_model(labels, logits):
    with tf.name_scope('xent'):
        loss = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(labels=labels,
                                                    logits=logits)
        )

    with tf.name_scope('train'):
        train_step = tf.train.AdamOptimizer().minimize(loss)

    with tf.name_scope('accuracy'):
        pred = tf.greater(tf.sigmoid(logits), 0.5)
        correct_pred = tf.equal(pred,
                                tf.cast(labels, tf.bool))
        accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

    return train_step, loss, accuracy, pred


def main():
    CELL_SIZE = 128
    CELL_CLASS = tf.nn.rnn_cell.LSTMCell
    N_LAYERS = 1

    BATCH_SIZE = 64
    EPOCHS = 1

    LOGS = True
    LOG_DIR_PATH = Path('../logs/sex_names')
    LOG_INTERVAL = 50  # log every {LOG_INTERVAL} mini-batches

    GET_CM = True
    CM_PATH = Path('../confusion_matrix.pdf')

    DATA_PATH = Path('../data/sex_names.csv')
    MODEL_DUMP_PATH = Path('../model_dumps')

    model_string = f'batch_{BATCH_SIZE}-' \
                   f'cell_{CELL_CLASS.__name__}-' \
                   f'units_{CELL_SIZE}-' \
                   f'layers_{N_LAYERS}'

    dataset = DataSet(path=DATA_PATH, one_hot=True, batch_size=BATCH_SIZE)

    X = tf.placeholder(tf.float32,
                       [None, *dataset.x_train.shape[1:]], name='inputs')
    Y = tf.placeholder(tf.float32, [None, 1], name='labels')

    logits = get_logits(X, CELL_SIZE, N_LAYERS, 1, CELL_CLASS)
    train_step, loss, accuracy, pred = get_model(Y, logits)

    batch_generator = dataset.get_batch_generator()

    tf.summary.scalar('Loss', loss)
    tf.summary.scalar('Accuracy', accuracy)
    write_op = tf.summary.merge_all()

    logs_base_path = LOG_DIR_PATH / f'{model_string}-ts_{int(time.time())}'

    writer_train = tf.summary.FileWriter(logs_base_path / 'train')
    writer_val = tf.summary.FileWriter(logs_base_path / 'val')

    test_feed = {X: dataset.x_test, Y: dataset.y_test}

    saver = tf.train.Saver()

    model_checkpoint_dir = MODEL_DUMP_PATH / model_string
    model_checkpoint = str(model_checkpoint_dir / 'model.ckpt')

    init = tf.global_variables_initializer()
    with tf.Session() as sess:

        if model_checkpoint_dir.exists():
            saver.restore(sess, model_checkpoint)
        else:
            sess.run(init)

        for e in tqdm(range(EPOCHS)):
            for i in tqdm(range(dataset.n_batches)):
                x_batch, y_batch = next(batch_generator)
                batch_feed = {X: x_batch, Y: y_batch}

                if LOGS and i % LOG_INTERVAL == 0:
                    step = e * dataset.n_batches + i

                    summary, _ = sess.run([write_op, train_step], batch_feed)
                    writer_train.add_summary(summary, step)

                    summary = sess.run(write_op, test_feed)
                    writer_val.add_summary(summary, step)

                else:
                    _ = sess.run(train_step, batch_feed)

        final_accuracy = sess.run(accuracy, test_feed)
        print('\nFinal model accuracy:', final_accuracy)

        saver.save(sess, model_checkpoint)

        if GET_CM:
            y_pred = sess.run(pred, feed_dict={X: dataset.x_test})
            cm = confusion_matrix(dataset.y_test, y_pred)

            names = ['Women', 'Man']
            plot_confusion_matrix(cm, classes=names,
                                  title='Confusion matrix')
            plt.savefig(CM_PATH, format='pdf')


if __name__ == '__main__':
    main()
