import string

import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split

from tensorflow.python.keras.utils import to_categorical


class DataSet:
    char_mapping = {char: i + 1 for i, char in
                    enumerate(string.ascii_lowercase)}
    sex_mapping = {'F': 0, 'M': 1}

    def __init__(self, path, one_hot=False,
                 batch_size=128,
                 test_size=0.25, random_state=42):
        self.raw_data = pd.read_csv(path)
        self.batch_size = batch_size

        self._max_seq_len = self.raw_data.name.map(len).max()
        self.sequences = self.raw_data.name.map(self._map_text_to_char_sequence)
        self.sequences = np.vstack(self.sequences)

        if one_hot:
            self.sequences = to_categorical(self.sequences)

        self.labels = self.raw_data.sex.map(self.sex_mapping) \
            .values.reshape([-1, 1])

        self.n_classes = np.unique(self.labels).shape[0]

        self.x_train, self.x_test, self.y_train, self.y_test = \
            self.get_train_test(test_size, random_state)

        self.num_train_samples = self.x_train.shape[0]
        self.n_batches = int(np.ceil(self.num_train_samples / batch_size))

    def get_train_test(self, test_size=0.25, random_state=42):
        X_train, X_test, y_train, y_test = \
            train_test_split(self.sequences, self.labels,
                             test_size=test_size, random_state=random_state)

        return X_train, X_test, y_train, y_test

    def _map_text_to_char_sequence(self, text):
        seq = np.array([self.char_mapping[char] for char in list(text.lower())])
        max_len_diff = self._max_seq_len - len(seq)

        if max_len_diff > 0:
            return np.hstack((np.zeros(max_len_diff), seq))

        return seq

    def get_batch_generator(self, batch_size=None):
        batch_size = batch_size if batch_size else self.batch_size

        while True:
            for split in self._get_random_splits(batch_size):
                yield self.x_train[split], self.y_train[split]

    def _get_random_splits(self, batch_size):
        arr = np.arange(self.num_train_samples)
        np.random.shuffle(arr)
        return np.split(arr, np.arange(self.num_train_samples,
                                       step=batch_size)[1:])
