from pathlib import Path
import string

from keras.layers import LSTM, Dense, Embedding
from keras.models import Sequential

from dataset import DataSet

if __name__ == '__main__':
    data_path = Path('../data/sex_names.csv')

    CELL_SIZE = 128
    USE_EMBEDDING = True

    dataset = DataSet(path=data_path, one_hot=not USE_EMBEDDING)

    X_train, X_test, y_train, y_test = dataset.get_train_test()

    if USE_EMBEDDING:
        model_head = [Embedding(len(string.ascii_lowercase) + 1, 32),
                      LSTM(CELL_SIZE)]
    else:
        model_head = [LSTM(CELL_SIZE, input_shape=X_train.shape[1:])]

    model = Sequential([
        *model_head,
        Dense(1, activation='sigmoid')
    ])

    model.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=['accuracy'])

    model.fit(x=X_train, y=y_train, batch_size=32, epochs=10,
              validation_data=(X_test, y_test))
